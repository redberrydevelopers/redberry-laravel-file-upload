# redberry/laravel-file-upload



## Installation

Add the repository:

```shell
$ composer config repositories.file-upload vcs git@bitbucket.org:redberrydevelopers/redberry-laravel-file-upload.git
```

Require the package in composer:

```shell
$ composer require redberry/laravel-file-upload:dev-master
```

Add the service provider to your config/app.php BEFORE your application service providers

```php
'providers' => [
    ...
    Redberry\FileUpload\FileUploadServiceProvider::class,

    /*
     * Application Service Providers...
     */
    App\Providers\AppServiceProvider::class,
    ...
],
```

Publish the package assets

```shell
$ php artisan vendor:publish
```

Run the migrations
```shell
$ php artisan migrate --path=database/migrations/vendor/redberry/laravel-file-upload
```

You should add an Apache alias to the storage/files folder
```
...
Alias /files /var/www/vhosts/marriott-events/staging/shared/storage/files
...
```

Also make sure this folder is server-writable.

## Usage