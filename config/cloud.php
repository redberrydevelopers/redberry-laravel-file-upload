<?php
/**
 * Created by PhpStorm.
 * User: Oscar
 * Date: 17/08/2015
 * Time: 10:15
 */

return [

    'enabled' => env('FILE_CLOUD_ENABLED', false),

    'client' => 'rackspace',

    'clients' => [

        'rackspace' => [
            'username' => '{username}',
            'apiKey' => '{apiKey}',
        ],

    ],

    'container' => env('FILE_CLOUD_CONTAINER'),

];