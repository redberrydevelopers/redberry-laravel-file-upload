<?php
/**
 * Created by PhpStorm.
 * User: Oscar
 * Date: 28/07/2015
 * Time: 14:30
 */

return [

    'path' => env('FILE_STORE_PATH', storage_path('files')),

    'url' => env('FILE_STORE_URL', 'files'),

];