<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCloudUrlToFileRecords extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('file_records', function(Blueprint $table) {
            $table->string('cloud_url')->nullable()->after('filesize');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('file_records', function(Blueprint $table) {
            $table->dropColumn('cloud_url');
        });
    }
}
