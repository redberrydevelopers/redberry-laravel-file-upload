<?php
/**
 * Created by PhpStorm.
 * User: Oscar
 * Date: 22/07/2015
 * Time: 14:23
 */

namespace Redberry\FileUpload;

use Illuminate\Foundation\AliasLoader;
use Illuminate\Support\ServiceProvider;

class FileUploadServiceProvider extends ServiceProvider {

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    public function boot() {
        /*
         * Load views
         */
        $this->app['view']->addNamespace('file-upload', __DIR__.'/../../../views');

        /*
         * Publish config
         */
        $this->publishes([
            __DIR__.'/../../../config' => config_path('file-upload'),
        ]);

        /*
         * Copy migrations
         */
        $this->publishes([
            __DIR__.'/../../../database/migrations/' => database_path('migrations/vendor/redberry/laravel-file-upload')
        ], 'migrations');
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register() {

    }

}