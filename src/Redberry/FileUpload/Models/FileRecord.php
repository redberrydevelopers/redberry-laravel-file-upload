<?php
/**
 * Created by PhpStorm.
 * User: Oscar
 * Date: 07/08/2015
 * Time: 11:24
 */

namespace Redberry\FileUpload\Models;


use Illuminate\Database\Eloquent\Model;
use Redberry\FileUpload\Upload\FileStore;
use Redberry\Utilities\Formatters\FileSize;

class FileRecord extends Model {
    
    public function getUrl() {
        if($this->cloud_url && config('file-upload.cloud.enabled')) {
            return $this->cloud_url;
        }
        else {
            return FileStore::fileRecordUrl($this);
        }
    }

    public function getFilePath() {
        return FileStore::fileRecordFilePath($this);
    }

    public function formatFilesize() {
        return FileSize::format($this->filesize);
    }

}