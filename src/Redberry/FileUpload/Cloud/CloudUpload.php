<?php
/**
 * Created by PhpStorm.
 * User: Oscar
 * Date: 13/08/2015
 * Time: 17:47
 */

namespace Redberry\FileUpload\Cloud;


use Guzzle\Http\Exception\ClientErrorResponseException;
use OpenCloud\ObjectStore\Constants\UrlType;
use OpenCloud\ObjectStore\Resource\DataObject;
use Redberry\FileUpload\Models\FileRecord;
use Redberry\FileUpload\Upload\FileStore;

class CloudUpload {

    public static function uploadFileRecord(FileRecord $fileRecord) {
        // Name of object in the container
        $objectName = FileStore::fileRecordObjectName($fileRecord);

        // File to upload
        $filePath = $fileRecord->getFilePath();

        return self::uploadFileToCloud($objectName, $filePath);
    }

    protected static function uploadFileToCloud($objectName, $filePath) {
        $container = self::getCreateContainer();

        // Check if there's an object with the same name already on there
        if($container->objectExists($objectName)) {
            /** @var DataObject $object */
            $object = $container->getPartialObject($objectName);

            // If the object exists, but the hash doesn't match then throw an exception
            if($object->getEtag() !== md5_file($filePath)) {
                throw new \Exception('File conflict on object storage: '.$container->name.'/'.$objectName);
            }
        }
        else {
            // Upload new object
            $object = $container->uploadObject($objectName, fopen($filePath, 'r+'));
        }

        return (string)$object->getPublicUrl(UrlType::SSL);
    }

    protected static function getCreateContainer() {
        $objectStore = OpenStackServiceFactory::getObjectStorageService();

        $containerName = config('file-upload.cloud.container');

        try {
            // Try get the container
            $container = $objectStore->getContainer($containerName);
        }
        catch(ClientErrorResponseException $exception) {
            // HTTP error

            if($exception->getResponse()->getStatusCode() === 404) {
                // It's a 404 - container doesn't exist so create it with CDN enabled
                $container = $objectStore->createContainer($containerName, [ 'X-CDN-Enabled' => 'True' ]);
            }
            else {
                // Another HTTP error - throw the exception
                throw $exception;
            }
        }

        return $container;
    }

}