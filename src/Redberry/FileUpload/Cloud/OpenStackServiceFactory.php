<?php
/**
 * Created by PhpStorm.
 * User: Oscar
 * Date: 14/08/2015
 * Time: 10:49
 */

namespace Redberry\FileUpload\Cloud;


use OpenCloud\Rackspace;

class OpenStackServiceFactory {

    public static function getObjectStorageService() {
        $clientName = config('file-upload.cloud.client');

        if($clientName === 'rackspace') {
            $client = new Rackspace(Rackspace::UK_IDENTITY_ENDPOINT, [
                'username' => config('file-upload.cloud.clients.rackspace.username'),
                'apiKey' => config('file-upload.cloud.clients.rackspace.apiKey'),
            ]);

            $objectStore = $client->objectStoreService(null, 'LON');
        }
        else {
            throw new \Exception('Invalid client: '.$clientName);
        }

        return $objectStore;
    }

}