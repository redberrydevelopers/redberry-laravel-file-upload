<?php
/**
 * Created by PhpStorm.
 * User: Oscar
 * Date: 07/08/2015
 * Time: 11:29
 */

namespace Redberry\FileUpload\Upload;


use Carbon\Carbon;
use Redberry\FileUpload\Models\FileRecord;
use Symfony\Component\HttpFoundation\File\Exception\FileException;

class FileStore {

    public static function path() {
        return rtrim(config('file-upload.store.path'), '\\/');
    }

    public static function url() {
        return rtrim(config('file-upload.store.url'), '\\/');
    }

    public static function datePath(Carbon $date) {
        return self::path().'/'.self::staggerDate($date);
    }

    public static function dateFilePath(Carbon $date, $name) {
        return self::datePath($date).'/'.$name;
    }

    public static function uniqueDateFilePath(Carbon $date, $name) {
        $file = self::dateFilePath($date, $name);

        if(file_exists($file)) {
            $fileName = pathinfo($name, PATHINFO_FILENAME);
            $ext = pathinfo($name, PATHINFO_EXTENSION);

            // Append the time to the file
            $newName = $fileName.'-'.$date->format('H-i-s').'.'.$ext;

            // Send through this function again in case the new filename also exists!
            return self::uniqueDateFilePath($date, $newName);
        }
        else {
            return $name;
        }
    }

    public static function fileRecordPath(FileRecord $record) {
        return self::datePath($record->created_at);
    }

    public static function createImageRecordPath(FileRecord $record) {
        return self::createPath(self::fileRecordPath($record));
    }

    public static function fileRecordFilePath(FileRecord $record) {
        return self::dateFilePath($record->created_at, $record->name);
    }

    public static function createPath($path) {
        if(!file_exists($path)) {
            if(!mkdir($path, 0777, true)) {
                throw new FileException(sprintf('Unable to create the "%s" directory', $path));
            }
        }

        return $path;
    }

    public static function dateUrl(Carbon $date) {
        return self::url().'/'.self::staggerDate($date);
    }

    public static function dateFileUrl(Carbon $date, $name) {
        return self::dateUrl($date).'/'.$name;
    }

    public static function fileRecordUrl(FileRecord $record) {
        return asset(self::dateFileUrl($record->created_at, $record->name));
    }

    public static function fileRecordObjectName(FileRecord $record) {
        return self::dateFileUrl($record->created_at, $record->name);
    }

    public static function staggerDate(Carbon $date) {
        return $date->format('Y/m/d');
    }

}