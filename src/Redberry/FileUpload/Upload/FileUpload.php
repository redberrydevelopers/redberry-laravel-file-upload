<?php
/**
 * Created by PhpStorm.
 * User: Oscar
 * Date: 07/08/2015
 * Time: 11:29
 */

namespace Redberry\FileUpload\Upload;


use Illuminate\Http\Request;
use Redberry\FileUpload\Cloud\CloudUpload;
use Redberry\FileUpload\Models\FileRecord;

class FileUpload {

    public function upload(Request $request, $key) {
        if(!$request->hasFile($key)) {
            throw new \Exception('No file provided: '.$key);
        }

        $file = $request->file($key);

        // File record in DB
        $record = new FileRecord();

        // Set date so we can use it to build the store path
        $record->created_at = $record->freshTimestamp();

        // Get unique file name
        $record->name = FileStore::uniqueDateFilePath($record->created_at, $file->getClientOriginalName());

        // File info
        $record->filesize = $file->getSize();

        // Destination file path
        $dir = FileStore::createImageRecordPath($record);

        // Move into place
        $file->move($dir, $record->name);

        // Upload to cloud if enabled
        if(config('file-upload.cloud.enabled')) {
            $url = CloudUpload::uploadFileRecord($record);
            $record->cloud_url = $url;
        }

        // Write record to DB
        $record->save();

        return $record;
    }

}