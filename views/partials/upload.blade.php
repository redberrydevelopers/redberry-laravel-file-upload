<div class="file-upload file-upload-{{ $fileKey }}">
    @if(isset($file) && $file)
        <a class="ui label">
            {{ $file->name }} ({{ $file->formatFilesize() }})
        </a>
    @endif

    <br/><br/>

    <input type="file" name="{{ $fileKey }}">
</div>